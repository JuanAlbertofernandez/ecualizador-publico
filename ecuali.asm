; primera aproximacion ecualizador.
; solo quiero ver si puedo pasarlo de gitlab a local
; ahora desde local a gitlab
; ahora lo edito desde el editor de gitlab

            output "ecualiza.rom"

            defpage 1,#4000,#4000
            defpage 3,#c000,#4000

            page 1            

            db "AB"
            dw start
            ds 12,00

start
           di 
           im 1
           ld sp,#f380

            ld a,1
            call #5f

            ld a,"A"
            ld hl,6144
            ld bc,512

            call #56

        ; inicializa

            xor a
            ld (contador_secuencia),a 

        ; define tiles

        ld hl,datos_tiles_0
        ld de,0
        ld bc,8

        call #5c

        ld hl,datos_tiles_8
        ld de,8*8
        ld bc,8

        call #5c

        ld hl,datos_tiles_16
        ld de,16*8
        ld bc,8

        call #5c

        ld hl,datos_tiles_24
        ld de,24*8
        ld bc,8

        call #5c

        ; color tiles

        ld hl,datos_color_tiles
        ld de,8192
        ld bc,4

        call #5c
           
            ei
            
loop
            call recorre_secuencia_datos
    [2]        halt
            call vuelca_a_vram
            

            jr loop


            ; datos_ecualizador tabla X8
tabla_datos
            db 0,0,0,0,0,0,0,0
            db 8,0,0,0,0,0,0,0
            db 8,8,0,0,0,0,0,0
            db 8,8,8,0,0,0,0,0
            db 8,8,8,8,0,0,0,0
            db 8,8,8,8,16,0,0,0
            db 8,8,8,8,16,16,0,0
            db 8,8,8,8,16,16,24,0
            db 8,8,8,8,16,16,24,24


recorre_secuencia_datos

            ld a,(contador_secuencia)
            ld l,a
            ld h,0
            ld de,secuencia_datos
            add hl,de
            ld a,(hl)
            
            ld l,a
            ld h,0

                        
[3]         add hl,hl
            
            ld de,tabla_datos

            add hl,de
            
            ld de,buffer_canal_0
            ld bc,8

            ldir
            

            ld hl,contador_secuencia
            ld a,(hl)
            inc a
            cp 32
            jr nz,incrementa
resetea
            xor a
incrementa            
            ld (hl),a
            ret 

vuelca_a_vram

            ld b,8
            ld de,buffer_canal_0
            ld hl,6144+23*32+11
lop
            push bc
                push de

            
                    push hl
                
                    ld a,(de)
                    call #4d

                    pop hl

                ld de,-32
                add hl,de
            
                pop de
                inc de

            pop bc
            djnz lop

            ret  



            ; define los tiles

datos_tiles_0

            ds 8,0
            
datos_tiles_8
            db 0
            db 126
            ds 4,255
            db 126
            db 0

datos_tiles_16
            db 0
            db 126
            ds 4,255
            db 126
            db 0

datos_tiles_24
            db 0
            db 126
            ds 4,255
            db 126
            db 0
datos_color_tiles
            db #44,#14,#a4,#94

secuencia_datos

            db 0,1,1,2,2,3,2,1,1,2,3,4,5,5,6,7,8,8,8,7,7,6,5,5,4,4,3,2,1,1,1,2

   
           
            page 3

contador_secuencia
            db 0

buffer_canal_0
            ds 8

           






